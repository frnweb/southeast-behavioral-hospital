<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'bM65Xb4GGXGVvxCh4u24Gs8aCq8bGIfu9YBaCMgJcYiJA028t6qGyvy8vUQiw5Cdd6bnRGxAaLhUIt520eG5AA==');
define('SECURE_AUTH_KEY',  'RKe9OM1ifAhcXRlji28i998zmociKZIBoGSi34kkIDEryJIRD8tEG7suaov56uGE4LdLGiPd1X4AW/Wyb6zRmQ==');
define('LOGGED_IN_KEY',    'nq1+K0cppnquqhCa2xD/lFlNbDD+b27caDcmsX0MOz8ztEBdFfTC7LyLsdBmG/pAWqFtR0aVDuvgi8cSmpDysw==');
define('NONCE_KEY',        'hzpiFmynGz9lAn1Lhe1JvDM2AFAlhAfi+90CursAjCVLU1P5M/YmX8Y3YhEj/hSRybDnvQ4ximZkVqHzJlZhIQ==');
define('AUTH_SALT',        'YXdpZRARl5OoNPsCJCUXG7D9+9vgmc0VFjmQFof+6xvcdsmTXFdwYM3gs6lVv8KuokGDQhSavZAxc0a5C2sTYA==');
define('SECURE_AUTH_SALT', '68gFcPrY7j+F027sAVKROZ/HkvL3HL7WApgkIzz2lrKzrgLFIffqWtg7ldXPcZOSUDUFuhE2KEZNtT+G/RNHFA==');
define('LOGGED_IN_SALT',   'RNJitxhQjGTV7ul2WU5MbkGWFsBhJ/zr6aTXgEXkzzg0fC4mAD1NIf6y9Z9VXlLzfSXAB6Vbmr3cyou5S3gKag==');
define('NONCE_SALT',       'wqanCiemLxuSco94gV5EdX2Vpb6okfF+r+/kGSecquhBxkYCgyT4tHkE5HWvxd4yHWfotNXQLTvKIzVOa/QebQ==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

// This enables debugging.
define( 'WP_DEBUG', true );




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
