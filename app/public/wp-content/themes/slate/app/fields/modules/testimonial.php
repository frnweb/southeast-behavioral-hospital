<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
	'ui' => 1,
	'wrapper' => ['width' => 30],
];

$testimonial = new FieldsBuilder('testimonial');

$testimonial
	->addTab('settings', ['placement' => 'left'])
		->addFields(get_field_partial('partials.add_class'))
		->addFields(get_field_partial('partials.module_title'));

$testimonial
	->addTab('media', ['placement' => 'left'])
		->addSelect('media', [
			'label' => 'Media Type',
			'wrapper' => ['width' => 20]
		])
	  	->addChoices(
		  ['image' => 'Image'],
		  ['video' => 'Video'],
		  ['gallery' => 'Gallery']
		)

		->addSelect('media_position', [
			'label' => 'Media Position',
			'wrapper' => ['width' => 20]
		])
	  	->addChoices(
		  ['left' => 'Media Left'],
		  ['right' => 'Media Right']
		)


 		//Image 
		->addImage('testimonial_image', ['wrapper' => ['width' => 60]])

		//Video
        ->addWysiwyg('video_embed', [
            'label' => 'Paste Video Embed Shortcode/iFrame',
        	])
        	->setInstructions('[embed src="embed url goes here"]')
		->conditional('media', '==', 'video' )

	->addTab('content', ['placement' => 'left'])

		//Header		
		->addTrueFalse('header_check', [
			'wrapper' => ['width' => 15],
			'label' => 'Add Header?'
			])
		->addText('header', [
			'label' => 'Pre-header',
			'wrapper' => ['width' => 85]
		])
			->setInstructions('Optional section header above testimonials' )
		->conditional('header_check', '==', 1)

    	//Repeater
		->addRepeater('testimonial', [
		  'min' => 1,
		  'max' => 6,
		  'button_label' => 'Add Item',
		  'layout' => 'block',
		])

		// Quote
		->addWysiwyg('quote', [
			'label' => 'Quote',
			'ui' => $config->ui
		])

		// Site
		->addText('cite', [
			'label' => 'Cite',
			'ui' => $config->ui
		]); 	

return $testimonial;